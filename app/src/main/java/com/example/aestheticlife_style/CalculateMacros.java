package com.example.aestheticlife_style;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class CalculateMacros extends AppCompatActivity{
    //Youtube loader
    RecyclerView recyclerView;
    Vector<YoutubeVideos> youtubeVideos = new Vector<>();

    //Loading data via Fire base
    private static final String TAG = "ViewMacros";
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private DatabaseReference databaseReference;
    private ListView myListView;

    //Calculating Macros
    private Button btnPlus;
    private Button btnMinus;
    private TextView calcText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_macros);
        //Youtube
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        //Calculating macros
        btnPlus = (Button)findViewById(R.id.btnGain);
        btnMinus = (Button)findViewById(R.id.btnShred);
        calcText = (TextView)findViewById(R.id.CalcText);

        //Accessing the database
        myListView = (ListView)findViewById(R.id.listView);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        FirebaseUser user = firebaseAuth.getCurrentUser();

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    //User is registered
                    Log.d(TAG, "onAuthStateChanged : Registered" + user.getUid());
                }
                else{
                    Log.d(TAG, "onAuthStateChanged : Not Registered");
                }
            }
        };

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                showData(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void showData(final DataSnapshot dataSnapshot) {
        for(DataSnapshot ds : dataSnapshot.getChildren()) {
            UserClass Class = new UserClass();
            Class.setUsername(ds.getValue(UserClass.class).getUsername()); //Set Username
            Class.setWeight(ds.getValue(UserClass.class).getWeight()); //Set Weight

            final String TotalWeight = Class.getWeight();

            btnPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick (View v){
                    String TotPl = TotalWeight;
                    int Totals = Integer.parseInt(TotPl);
                    if (Totals <= 70) {
                        int Total = 2500 + 500;
                        calcText.setText(Integer.toString(Total));

                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/ZYJqTNvv1Ys\" frameborder=\"0\" allowFullScreen></iframe>"));
                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/dinpkrcpKAk\" frameborder=\"0\" allowFullScreen></iframe>"));
                        VideoAdapter videoAdapter = new VideoAdapter(youtubeVideos);
                        recyclerView.setAdapter(videoAdapter);
                    } else if (Totals >= 70) {
                        int Total = 2700 + 300;
                        calcText.setText(Integer.toString(Total));

                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/ZYJqTNvv1Ys\" frameborder=\"0\" allowFullScreen></iframe>"));
                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/dinpkrcpKAk\" frameborder=\"0\" allowFullScreen></iframe>"));
                        VideoAdapter videoAdapter = new VideoAdapter(youtubeVideos);
                        recyclerView.setAdapter(videoAdapter);
                    } else if (Totals > 100) {
                        int Total = 3000 + 100;
                        calcText.setText(Integer.toString(Total));

                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/ZYJqTNvv1Ys\" frameborder=\"0\" allowFullScreen></iframe>"));
                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/dinpkrcpKAk\" frameborder=\"0\" allowFullScreen></iframe>"));
                        VideoAdapter videoAdapter = new VideoAdapter(youtubeVideos);
                        recyclerView.setAdapter(videoAdapter);
                    }
                }
            });

           btnMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick (View v){
                    String TotMn = TotalWeight;
                    int Totals = Integer.parseInt(TotMn);
                    if (Totals <= 70) {
                        int Total = 2500 - 100;
                        calcText.setText(Integer.toString(Total));

                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/CxktmQ3zJOA\" frameborder=\"0\" allowFullScreen></iframe>"));
                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/1GqoX6Dk5ko\" frameborder=\"0\" allowFullScreen></iframe>"));
                        VideoAdapter videoAdapter = new VideoAdapter(youtubeVideos);
                        recyclerView.setAdapter(videoAdapter);
                    } else if (Totals >= 70) {
                        int Total = 2700 - 300;
                        calcText.setText(Integer.toString(Total));

                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/CxktmQ3zJOA\" frameborder=\"0\" allowFullScreen></iframe>"));
                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/1GqoX6Dk5ko\" frameborder=\"0\" allowFullScreen></iframe>"));
                        VideoAdapter videoAdapter = new VideoAdapter(youtubeVideos);
                        recyclerView.setAdapter(videoAdapter);
                    } else if (Totals > 100) {
                        int Total = 3000 - 500;
                        calcText.setText(Integer.toString(Total));

                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/CxktmQ3zJOA\" frameborder=\"0\" allowFullScreen></iframe>"));
                        youtubeVideos.add(new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/1GqoX6Dk5ko\" frameborder=\"0\" allowFullScreen></iframe>"));
                        VideoAdapter videoAdapter = new VideoAdapter(youtubeVideos);
                        recyclerView.setAdapter(videoAdapter);
                    }
                }
            });

            //Display information
            Log.d(TAG, "showData: Username: " + Class.getUsername());
            Log.d(TAG, "showData: Weight: " + Class.getWeight());

            ArrayList<String> array = new ArrayList<>();
            array.add(Class.getUsername());
            array.add(Class.getWeight());
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,array);
            myListView.setAdapter(adapter);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }
    @Override
    public void onStop(){
        super.onStop();
        if(authStateListener != null){
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }

    private void toastMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
