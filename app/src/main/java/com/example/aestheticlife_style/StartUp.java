package com.example.aestheticlife_style;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

public class StartUp extends AppCompatActivity {

    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        pb = (ProgressBar) findViewById(R.id.progress_bar);

        new LoadScreenTask().execute();
    }

    class LoadScreenTask extends AsyncTask<Void, Integer, Integer>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //Update Progressor
            pb.setMax(100);
            Toast.makeText(getApplicationContext(),"Application launching...",Toast.LENGTH_SHORT).show();
        }

        @Override
        protected  void onProgressUpdate(Integer... values){
            super.onProgressUpdate(values);

            //Update the progress bar
            pb.setProgress(values[0]);
        }

        @Override
        protected Integer doInBackground(Void... arg0) {
            for(int i=0; i<100; i++) {
                publishProgress(i);
                try
                {
                    Thread.sleep(100);
                } catch (InterruptedException ex)
                {
                    ex.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);

            Toast.makeText(getApplicationContext(),"Entering Application",Toast.LENGTH_SHORT).show();
            Load();
        }
    }

    public void Load()
    {
        Intent intent = new Intent(this, PinView.class);
        startActivity(intent);
    }
}
