package com.example.aestheticlife_style;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class PinView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pinview);

        Pinview pinview = (Pinview)findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {
                if(pinview.getValue().equals("1234")){
                    Toast.makeText(PinView.this,"Pin is correct!", Toast.LENGTH_SHORT).show();
                    Load();
                }
                else
                {
                    Toast.makeText(PinView.this,"Pin is not valid, retry!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void Load()
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
