package com.example.aestheticlife_style;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.jar.Attributes;



public class MainMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;

    //Register
    private Button RegButton;
    private EditText UserName;
    private EditText Weigth;
    DatabaseReference databaseReference;

    //Shared Preferences
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String TEXT = "text";
    public static final String TEXT2 = "text2";

    private String text;
    private String text2;

    private String dateAndTime = DateFormat.getDateTimeInstance().format(new Date());
    Date currentTime = Calendar.getInstance().getTime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //Registration
        databaseReference = FirebaseDatabase.getInstance().getReference();

        RegButton = (Button) findViewById(R.id.regButton);
        UserName = (EditText) findViewById(R.id.fillInUser);
        Weigth = (EditText) findViewById(R.id.fillInWeight);

        RegButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        //Nav Bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toogler = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.nav_draw_open, R.string.nav_draw_close);

        drawer.addDrawerListener(toogler);
        toogler.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Shared Prefs
        loadData();
        updateViews();
    }


    //Register
    private void registerUser() {
        String personUser = UserName.getText().toString();
        String personWeight = Weigth.getText().toString();

        //If validations are correct
        if (!TextUtils.isEmpty(personUser) && !TextUtils.isEmpty(personWeight)) {
            String id = databaseReference.push().getKey();
            UserClass userClass = new UserClass(id, personUser, personWeight);

            databaseReference.child(id).setValue(userClass);
            Toast.makeText(this, "User Registered", Toast.LENGTH_SHORT).show();
            UserName.setText("");
            Weigth.setText("");
        } else {
            //If there is a problem
            Toast.makeText(this, "Unable to register, check user or weight", Toast.LENGTH_SHORT).show();
        }
    }

    //Nav Bar
    public void OnNavBackPress()
    {
        if(drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatmentWithEmptyBody")
    public boolean onNavigationItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if(id == R.id.next_page1) {
        Intent i = new Intent(this, Exercises.class);
        startActivity(i);
        }
        else if(id == R.id.next_page2) {
            Intent i = new Intent(this, CalculateMacros.class);
            startActivity(i);
        }
        else if(id == R.id.next_page3) {
            Intent i = new Intent(this, TypicalFood.class);
            startActivity(i);
        }
        else if(id == R.id.next_page4) {
            Intent i = new Intent(this, HowMuchTrain.class);
            startActivity(i);
        }
        return true;
    }

    @Override
    protected void onStart(){
        super.onStart();
        loadData();
        showSnackbar();
    }

    @Override
    protected void onResume(){
        super.onResume();
        loadData();
        showSnackbar();
    }

    @Override
    protected void onPause(){
        super.onPause();
        storeData();
        Toast.makeText(this, dateAndTime, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop(){
        super.onStop();
        storeData();
        Toast.makeText(this, dateAndTime, Toast.LENGTH_SHORT).show();
    }

    public void storeData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(TEXT, UserName.getText().toString());
        editor.putString(TEXT2, Weigth.getText().toString());

        editor.apply();
        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
    }

    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT, "Empty Field");
        text2 = sharedPreferences.getString(TEXT2, "Empty Field");
    }

    public void updateViews(){
        UserName.setText(text);
        Weigth.setText(text2);
    }

    public void showSnackbar(){
        Snackbar snackbar = Snackbar.make(drawer, "Last time logged in was: "+ dateAndTime, Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }
}