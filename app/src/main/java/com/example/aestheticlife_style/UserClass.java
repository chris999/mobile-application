package com.example.aestheticlife_style;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.EditText;

public class UserClass {
    private String Id;
    private String Username;
    private String Weight;

    public UserClass(){

    }

    public UserClass(String Id, String Username, String Weight)
    {
        this.Id = Id;
        this.Username = Username;
        this.Weight = Weight;
    }

    public String getId(){
        return Id;
    }
    public String getUsername(){
        return Username;
    }
    public void setUsername(String Username){
        this.Username = Username;
    }
    public String getWeight(){
        return Weight;
    }
    public void setWeight(String Weight){
        this.Weight = Weight;
    }

}
