package com.example.aestheticlife_style;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class Exercises extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);
    }

    public void Chest(View v){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=rT7DgCr-3pg")));
        Log.i("Video", "Video Playing....");
    }
    public void Back(View v){
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=ytGaGIn3SjE")));
        Log.i("Video", "Video Playing....");
    }
    public void Legs(View v){
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=aclHkVaku9U")));
        Log.i("Video", "Video Playing....");
    }
    public void Shoulders(View v){
        startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.youtube.com/watch?v=QAQ64hK4Xxs")));
        Log.i("Video", "Video Playing....");
    }
}
